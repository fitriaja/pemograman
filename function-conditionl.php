<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Function PHP</h1>
    <?php

    echo "<h3> Soal No 1 Greetings </h3>";
    /*
    Soal No 1
    Greetings
    Buatlah sebuah function greeting() yang menerima satu parameter berupa string.

    contoh: greeting ("abduh");
    Output: "Halo Abduh, Selamat Datang di AMIK Selatpanjang!"
    */

    // Code fuunction di sini
    function greetings($nama){
        echo "Halo " . $nama . ", Selamat datang <br>";
    }

    greetings("Bagas");
    greetings("Wahyu");
    greetings("Abdul");

    echo "<br>";

    // Hapus komentar untuk menjadikan code!
    // greetings("Bagas");
    // greetings("Wahyu");
    // greetings("nama mahasiswa");

    echo "<br>";

    echo "<h3>Soal No 2 Reverse String</h3>";
    /*
    Soal No 2
    Reverse String
    Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping (for/while/do whilue).
    Function reveseString menerima satu parameter berupa string.
    NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

    reverseString("abdul");
    Output: ludba

    */

    // Code function di sini
        function reverse($skala){
            $panjangkata = string($kata1);
            $tampung = "";
            for ($i=($panjangkata - 1) ; $i>=0; $i--){
                $tampung .= $kata1[$i];
            }
            return $tampung;
        }
        function reverseString($kata2){
            $string=reverse($kata);
            echo $string . "<br>";

        }

        reverseString("nama mahasiswa");
        reverseString("AMIK Selatpanjang");
        reverseString("we Are AMIK Developers");
        echo "<br>";

    echo "<h3>Soal No 3 palindrome </h3>";
    /*
    Soal  No 3
    Palindrome
    Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan.
    Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
    Jika string tersebut plaindrome maka akan mengembalikam nilai true, sedangkan jika buka palindrime akan mengembalikan false.
    NB: 
    Contoh:
    palindrome("katak") => output : "true"
    palindrome("jambu") => output : "false"
    NB : DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!

    */

    // Code function di sini
    function palindrome($pali){
        $balikKata = reverse($pali);
        if($balikKata === $pali){
            echo "True <br>";
        }
        else {
            echo "False <br>";
        }
    }

    palindrome("civic") ; // true
    palindrome("nababan") ; // true
    paliindrome("jawababan") ; // false
    palindrome("racecar"); // true

    echo "<h3>Soal No 4 Tentukan Nilai </h3>";
    /*
    Soal 4
    buatlah sebuah function bernama tentukan_nilai . Di dalam function tentukan_nilai yang menerima parameter berupa integer. dengan ketentuan jika parameter integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String "Sangat Baik"
    Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string "Baik" selain itu jika parameter number lebih besar 
    sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string "Cukup" selain itu maka mereturn string "Kurang"
    */

    // Code function di sini
    function tentukan_nilai($angka){
        $output = "";
        if($angka)>=98 && $angka <=100{
            $output .= "Sangat Baik";
        }else if ($angka>=76 && $angka <98){
            $output .= "Baik";
        }else if ($angka>=67 && $angka <76){
            $output .= "Cukup";
        }lse if ($angka>=43 && $angka <67){
            $output .= "Kurang";
        }
        return $output . "<br>";
    }
    echo tentukan_nilai(98); // Sangat Baik
    echo tentukan_nilai(76); // Baik
    echo tentukan_nilai(67); // cukup
    echo tentukan_nilai(43); // Kurang
    ?>

        
</body>
</html>